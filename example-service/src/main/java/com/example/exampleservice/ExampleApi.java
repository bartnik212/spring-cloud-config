package com.example.exampleservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExampleApi {

    @GetMapping
    public String hello() {
        return "hello from example";
    }
}
