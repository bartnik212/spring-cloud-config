package com.example.example2service;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Example2Api {

    @GetMapping
    public String hello() {
        return "hello from example2";
    }
}
