package com.example.example2service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class Example2ServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(Example2ServiceApplication.class, args);
    }

}
